import pathlib, time, re, pprint
import boards, similarity

def clean(
    v,
    unwanted=r'[^a-zäöåA-ZÅÄÖ0-9\-\s\.!\?\u20AC]',
    lower=True
):
    """Cleans and restructures tokens."""
    f_re = lambda s: (
        re.sub(r'\s+', ' ',
                re.sub(unwanted, ' ',
                    re.sub(r'[\.|!|\?]+', r' . ',
                        re.sub(r'(\u20AC)+', r'\1 ', s)
                    )
                )
        ).split(" ")
    )

    if (type(v) == str):
        if (lower):
            t = str.lower(v)
        else:
            t = v
        #

        return f_re(t)
    else:
        return [
            f_re(text)

            for text in list(
                map(lambda t: str.lower(t) if lower else t, v)
            )
        ]
    #
#

def filter_tokens(v, rules):
    """Filters tokens based on a given ruleset."""
    t0 = time.time()

    print(f"Removing tokens ({rules})...")

    filtered_tokens = []

    rule_test = lambda t: False if (
        ("min_n" in rules and rules["min_n"] >= len(t))
        or
        ("max_n" in rules and rules["max_n"] <= len(t))
        or
        ("no_digits" in rules and t.isdigit())
    ) else True

    for row in v:
        filtered_tokens.append([
            t
            for t in row if (rule_test(t))
        ])
    #

    tf = time.time()
    print(f"Removed ... tokens in {tf - t0} s.")

    return filtered_tokens
#

def read_wsf(filename):
    """Reads a whitespace split file."""
    with open(f"data/info/{filename}", "r") as f:
        rows = f.read().split("\n")
    #

    clean_terms = [
        str.lower(row).split(" ")
        for row in rows
    ]

    return clean_terms
#

def export(terms, filename):
    with open(f"data/exported/{filename}", "w") as f:
        for term in terms:
            f.write(" ".join(term) + "\n")
        #
    #
#

def export_matrix(matrix, drug_terms, hood_terms, filename):
    x_header, y_header = [
        row[0]
        for row in drug_terms
    ], [
        row[0]
        for row in hood_terms
    ]

    with open(f"data/exported/{filename}", "w") as f:
        f.write("x-header: " + " ".join(x_header) + "\n")
        f.write("y-header: " + " ".join(y_header) + "\n")

        m, n = len(y_header), len(x_header)
        for y, row in enumerate(matrix):
            for x, cell in enumerate(row):
                f.write(str(cell))

                if (x + 1 < n):
                    f.write(" ")
                else:
                    if (y + 1 < m):
                        f.write("\n")
                    #
                #
            #
        #
    #
#

def proc(args):
    rules = {"min_n": 2, "max_n": 16, "nodigits": True}

    texts = filter_tokens(
        clean(boards.read("tre", args.date, args.time)),
        rules
    )

    drug_terms, hood_terms = read_wsf("tre-drug_names.txt"),
        read_wsf("tre-hood_names.txt")

    matrix = similarity.in_compare(texts, drug_terms, hood_terms)
    export_matrix(matrix, drug_terms, hood_terms, args.output)
#

def main(args):
    proc(args)
#

if (__name__ == "__main__"):
    import argparse

    parser = argparse.ArgumentParser(
        description=(
            "Valitsee käsiteltävät keskustelut annettujen aikakriteerien "
            "perusteella."
        )
    )

    parser.add_argument("--time", default="*", type=str,
        help=(
            "Tunteina ilmaistu aikaväli (esim.: 20-23)."
        )
    )

    parser.add_argument("--date", default="*", type=str,
        help=(
            "Päivämäärinä ilmaistu aikaväli (esim.: 2019.02.20-2019.03.20)."
        )
    )

    parser.add_argument("--output", default="hood-drug_matrix.txt", type=str,
        help=(
            "data/export/-kansioon tallennettavan matriisin tiedostonimi."
        )
    )

    main(parser.parse_args())
#
