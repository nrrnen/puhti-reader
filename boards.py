import pathlib

def read(city, f_date, f_time):
    BASE_PATH = f"data/{city}/"

    print(
        f"Reading files from {BASE_PATH} with filters "
        f"(date: {f_date}, time: {f_time})..."
    )

    data_folder = pathlib.Path(BASE_PATH)
    
    if (not data_folder.is_dir()):
        sys.exit(f"error: {BASE_PATH} not found or not a directory")
    #

    if (f_date == "*" and f_time == "*"):
        texts = read_all(data_folder)
    else:
        d0, df, h0, hf = None, None, None, None
        
        if (f_date != "*"):
            tmp = f_date.split("-")

            d0, df = datetime.date(
                *list(map(int, tmp[0].split(".")))
            ), datetime.date(
                *list(map(int, tmp[1].split(".")))
            )

            if (d0 > df):
                sys.exit(f"error: wonky date range: {d0} > {df}.")
            #
        #

        if (f_time != "*"):
            h0, hf = list(map(int, f_time.split("-")))

            if (h0 > hf):
                sys.exit(f"error: wonky time range: {h0} > {hf}.")
            #
        #

        texts = read_filter(data_folder, f_date, f_time, d0, df, h0, hf)
    #

    print(f"Read {len(texts)} texts from {BASE_PATH}.")

    return texts
#

def read_all(data_folder):
    texts = []

    for file in data_folder.iterdir():
        posts = file.read_text().split("\n")
        
        for post in posts:
            cells = post.split("\t")

            texts.append(" . ".join([cells[3], cells[4]]))
        #
    #

    return texts
#

def read_filter(data_folder, f_date, f_time, d0, df, h0, hf):
    texts = []

    for file in data_folder.iterdir():
        posts = file.read_text().split("\n")

        for post in posts:
            cells = post.split("\t")
            times = cells[1].split(" ")

            if (len(times) != 3):
                sys.exit(f"error: corrupted message row: {chrono}.")
            #

            date_flag, time_flag = True, True

            if (f_date != "*"):
                d = datetime.date(*list(map(int, times[0].split("-"))))

                if (not d0 < d < df):
                    date_flag = False
                #
            #

            if (f_time != "*"):
                h = int(times[2].split(":")[0])

                if (not h0 < h < hf):
                    time_flag = False
                #
            #

            if (date_flag and time_flag):
                texts.append(" . ".join([cells[3], cells[4]]))
            #
        #
    #

    return texts
#
