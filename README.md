# PUHTI-reader

Usage: python tor-rdr.py

Luo tiedoston data/exported/hood-drug_matrix.txt

Valinnaisia parametreja:

python tor-rdr.py --time 20-23 --date 2019.02.20-2019.03.20 --output "tulostematriisi.txt"

time:  Noudattaa muotoa h0-hf (keskiyö on 00).

date: Noudattaa muotoa y0.m0.d0-yf.mf.df.

output: Vapaavalintainen tiedostonimi, oletuksena "hood-drug_matrix.txt".

Tulostematriisi sisältää päihteiden esiintymisfrekvenssit per kaupunginosa. Kaupunginosat on jaettu riveille ja frekvenssit soluihin.