import textdistance, difflib
from similarity_libraries.jarowinkler import JaroWinkler
import pprint

def similarity_compare(texts, method, THRESHOLD, drug_terms, hood_terms):
    """Uses a string similarity function to compare terms.
    Returns a (hood, drug) matrix (h, w)."""
    hoods, other_words = [], set([])

    for term in hood_terms:
        hood = [
            0
            for _ in drug_terms
        ]

        hoods.append(hood)
    #

    similarity_f = [
        textdistance.ratcliff_obershelp,
        textdistance.hamming.normalized_similarity,
        JaroWinkler().similarity,
        lambda w, t: difflib.SequenceMatcher(None, w, t).ratio()
    ][method]

    for text in texts:
        text_drugs, text_hoods = [], []

        text_match_d, text_match_h = False, False
        for word in text:
            if (word in other_words):
                continue
            #

            match_d, match_h = False, False

            for i, row in enumerate(drug_terms):
                if (word in row):
                    text_drugs.append(i)

                    match_d = True
                    text_match_d = True
                    break
                #

                for term in row:
                    if (similarity_f(word, term) > THRESHOLD):
                        text_drugs.append(i)

                        match_d = True
                        text_match_d = True
                        break
                    #
                #

                if (match_d):
                    break
                #
            #

            if (match_d):
                continue
            #

            for i, row in enumerate(hood_terms):
                if (word in row):
                    text_hoods.append(i)

                    match_h = True
                    text_match_h = True
                    break
                #

                for term in row:
                    if (similarity_f(word, term) > THRESHOLD):
                        text_hoods.append(i)

                        match_h = True
                        text_match_h = True
                        break
                    #
                #

                if (match_h):
                    break
                #
            #

            if (match_d == False and match_h == False):
                other_words.add(word)
            #
        #

        if (text_match_d and text_match_h):
            for i in text_hoods:
                for j in text_drugs:
                    hoods[i][j] += 1
                #
            #
        #
    #

    return hoods
#

def in_compare(texts, drug_terms, hood_terms):
    """Checks if terms exist in given lists.
    Returns a (hood, drug) matrix (h, w)."""
    hoods, other_words = [], set([])

    for term in hood_terms:
        hood = [
            0
            for _ in drug_terms
        ]

        hoods.append(hood)
    #

    for text in texts:
        text_drugs, text_hoods = [], []

        text_match_d, text_match_h = False, False
        for word in text:
            if (word in other_words):
                continue
            #

            match_d, match_h = False, False

            for i, row in enumerate(drug_terms):
                if (word in row):
                    text_drugs.append(i)

                    match_d = True
                    text_match_d = True
                    break
                #
            #

            if (match_d):
                continue
            #

            for i, row in enumerate(hood_terms):
                if (word in row):
                    text_hoods.append(i)

                    match_h = True
                    text_match_h = True
                    break
                #
            #

            if (match_d == False and match_h == False):
                other_words.add(word)
            #
        #

        if (text_match_d and text_match_h):
            for i in text_hoods:
                for j in text_drugs:
                    hoods[i][j] += 1
                #
            #
        #
    #

    return hoods
#
